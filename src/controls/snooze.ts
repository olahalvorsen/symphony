import { Component } from '@angular/core';
import { ViewController, NavParams, ToastController } from 'ionic-angular';
import { FirebaseProvider } from '../providers/firebase/firebase';
import * as moment from 'moment';
import { todo } from '../data/todo';
import { unitOfTime } from 'moment';

// export enum snoozeTime {
//   laterToday,
//   tomorrow,
//   in3days,
//   thisWeekend,
//   nextWeek,
//   SomeTimeNextMonth,
//   SetDate
// }

@Component({
  templateUrl: 'snooze.html',
})
export class snoozePage {

  public currentTodo: todo;
  // public ChangedDueDate: string;

  constructor(public viewCtrl: ViewController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              private firebaseProvider: FirebaseProvider) {
    if (navParams.data["currentTodo"]) {
      this.currentTodo = navParams.data["currentTodo"];
    }

  }

  ionViewDidLoad() {
    this.firebaseProvider.getLists();
  }

  snooze(snoozeTime: string) {
    let currentDue = moment(this.currentTodo.due);

    moment(new Date());

    switch (snoozeTime) {
      case "laterToday": "";
      this.currentTodo.due = new Date(moment(new Date()).add(3, 'hours').format("YYYY-MM-DDTHH:mm"));
        break;
      case "tomorrow": "";
        this.currentTodo.due = new Date(moment(new Date()).add(1, 'days').format("YYYY-MM-DDTHH:mm"));
        break;
      case "in3days":
        this.currentTodo.due = new Date(moment(new Date()).add(3, 'days').format("YYYY-MM-DDTHH:mm"));
        break;
      case "thisWeekend":
      this.currentTodo.due = new Date(moment().endOf('week').format("YYYY-MM-DDTHH:mm"));
        break;
      case "nextWeek":
      this.currentTodo.due = new Date(moment(new Date()).add(7, 'days').startOf('isoweek' as unitOfTime.StartOf).format("YYYY-MM-DDTHH:mm"));
        break;
      case "nextMonth":
      this.currentTodo.due = new Date(moment(new Date()).add(1, 'months').startOf('month' as unitOfTime.StartOf).format("YYYY-MM-DDTHH:mm"));
        break;
      // case "setDate":
      //   alert("Not implemented yet");
      //   break;
      default:
        alert("Cannot find the snooze: " + snoozeTime);
        break;
    }

    this.firebaseProvider.saveTodoChanges(this.currentTodo);
    this.presentToast(this.currentTodo.header + " has been snoozed to: " + moment(this.currentTodo.due).format("D MMMM YYYY hh:mm"));
    this.viewCtrl.dismiss();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      position: 'bottom',
      message: message,
      duration: 3000
    });
    toast.present();
  }
}