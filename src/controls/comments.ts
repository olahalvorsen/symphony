import { Component, Input } from '@angular/core';
import { comment } from '../data/todo';

@Component({
    selector: 'comments',
    templateUrl: 'comments.html',
})
export class Comments {
    @Input() comments: comment[];
    constructor() {

    }
}