import { Component, Input } from '@angular/core';
import { todo } from '../data/todo';
import * as moment from 'moment';

@Component({
    selector: 'task-Info',
    templateUrl: 'taskInfo.html',
})
export class TaskInfo {
    @Input() todo: todo;

    constructor() {

    }

    get Overdue(): boolean {
        if (this.todo && this.todo.due) {
            let m1 = moment(this.todo.due);
            let m2 = moment(new Date());
            return m1.startOf('day').isBefore(m2.startOf('day'));
        } else {
            return false;
        }
    }

    get Today(): boolean {
        if (this.todo && this.todo.due) {
            let m1 = moment(this.todo.due);
            let m2 = moment(new Date());
            return m1.startOf('day').isSame(m2.startOf('day'));
        } else {
            return false;
        }
    }

    get Later(): boolean {
        if (this.todo && this.todo.due) {
            let m1 = moment(this.todo.due);
            let m2 = moment(new Date());
            return m1.startOf('day').isAfter(m2.startOf('day'));
        } else {
            return false;
        }
    }
}



