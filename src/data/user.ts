export interface User {
    uid: string
    email: string;
    photoURL?: string;
    firstName?: string;
    lastName?: string
}