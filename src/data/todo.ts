import {list} from './list';

export interface todo {
    header: string;
    id?: string;
    description?: string;
    complete: boolean;
    list: list; 
    due?: Date;
    comments: comment[];
    owner: string;
}

export interface comment {
    header: string;
    userName: string;
}