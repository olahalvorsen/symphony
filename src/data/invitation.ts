export class Invitation {
    id?: string;
    fromEmail: string;
    toEmail: string;
    listId: string;
    listName: string;
    accepted: boolean;
    declined: boolean;
}