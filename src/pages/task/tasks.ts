import { Component } from '@angular/core';
import { NavController, PopoverController, ToastController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ListChooserPage } from '../listControls/listChooserPage';
import { snoozePage } from '../../controls/snooze';
import { optionsPage } from '../task/optionsPage';
import { EditTask } from './editTask';
import { todo } from '../../data/todo';
import { list } from '../../data/list';

import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import * as moment from 'moment';

@Component({
  selector: 'page-tasks',
  templateUrl: 'tasks.html'
})
export class TasksPage {
  currentList: list = null;
  someString: string;
  quickTaskInput: string;

  userId: string;
  userImage: string;

  public filteredTodos: todo[];

  private showComplete: boolean = false;
  private sortByDate: boolean = true;

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private firebaseProvider: FirebaseProvider,
    private afAuth: AngularFireAuth) {
    this.someString = "Symphony Todo";

    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
        this.userImage = user.photoURL;
      }
    });
  }

  ionViewDidLoad() {
    this.firebaseProvider.getTodosForList(this.currentList, this.showComplete, this.sortByDate);
  }

  setItemStyle(todo: todo) {
    if(!todo.list) {
      return null;
    }
    if(!todo.list.colour) {
      todo.list.colour = 'transparent';
    }
    let styles = {
      'border-left': `8px solid ${todo.list.colour}`,
    };
    return styles;
  }

  // addTask() {
  //   let profileModal = this.modalCtrl.create(EditTask, { currentList: this.currentList });
  //   profileModal.onDidDismiss(newTodo => {
  //     if(newTodo) {
  //       this.firebaseProvider.createTodo(newTodo);
  //     }
  //   });

  //   profileModal.present();
  // }

  editTask(currentTask: todo) {
    let profileModal = this.modalCtrl.create(EditTask, { currentList: this.currentList, currentTask: currentTask });
    profileModal.onDidDismiss(editedTask => {
      if (editedTask) {
        this.saveChanges(editedTask);
      }
    });

    profileModal.present();
  }

  openListChooser(myEvent) {
    let popover = this.popoverCtrl.create(ListChooserPage, { showAll: true });
    popover.onDidDismiss(chosenList => {
      this.currentList = chosenList;
      this.firebaseProvider.getTodosForList(this.currentList, this.showComplete, this.sortByDate);
    });
    popover.present({
      ev: myEvent
    });
  }

  completeTask(changedTodo: todo) {
    this.saveChanges(changedTodo);
    if (changedTodo.complete) {
      this.presentToast(" The task: \"" + changedTodo.header + "\" has been completed!");
    }
  }

  saveChanges(changedTodo: todo) {
    this.firebaseProvider.saveTodoChanges(changedTodo);
  }

  deleteTodo(todoToDelete: todo) {
    this.firebaseProvider.deleteTodo(todoToDelete);
  }

  moveToList(myEvent, todo: todo) {
    let popover = this.popoverCtrl.create(ListChooserPage, { showAll: true });
    popover.onDidDismiss(chosenList => {
      todo.list = chosenList;
      this.firebaseProvider.saveTodoChanges(todo);
    });
    popover.present({
      ev: myEvent
    });
  }

  onQuickTaskEnter() {
    if (this.quickTaskInput) {
      var newTodo: todo = {
        header: this.quickTaskInput,
        description: null,
        complete: false,
        due: null,
        list: this.currentList ? this.currentList : null,
        comments: null,
        owner: this.userId
      };

      this.firebaseProvider.createTodo(newTodo);
      this.quickTaskInput = "";
    }
  }

  isNoDate(todo: todo) {
    if (todo.due) {
      return false;
    }
    else {
      return true;
    }
  }

  isOverdue(todo: todo) {
    if (todo.due) {
      let m1 = moment(todo.due);
      let m2 = moment(new Date());
      return m1.startOf('day').isBefore(m2.startOf('day'));
    }
  }

  isToday(todo: todo) {
    if (todo.due) {
      let m1 = moment(todo.due);
      let m2 = moment(new Date());
      return m1.startOf('day').isSame(m2.startOf('day'));
    }
  }

  isLater(todo: todo) {
    if (todo.due) {
      let m1 = moment(todo.due);
      let m2 = moment(new Date());
      return m1.startOf('day').isAfter(m2.startOf('day'));
    }
  }

  snooze(myEvent, todo: todo) {
    let popover = this.popoverCtrl.create(snoozePage, { currentTodo: todo  });
    popover.onDidDismiss(todoToSnooze => {
      //this.currentList = chosenList;
      //this.firebaseProvider.getTodosForList(this.currentList);
    });
    popover.present({
      ev: myEvent
    });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      position: 'bottom',
      message: message,
      duration: 3000
    });
    toast.present();
  }

  presentOrderingAndGroupingOptions(myEvent) {
    let popover = this.popoverCtrl.create(optionsPage, { showComplete: this.showComplete, sortByDate: this.sortByDate });
    popover.onDidDismiss(options => {
      if(options) {
        this.showComplete = options.showComplete;
        this.sortByDate = options.sortByDate;
        this.firebaseProvider.getTodosForList(this.currentList, this.showComplete, this.sortByDate);
      }
    });
    popover.present({
      ev: myEvent
    });
  }
}
