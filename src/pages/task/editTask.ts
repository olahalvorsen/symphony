import { Component } from '@angular/core';
import { ViewController, PopoverController, NavParams } from 'ionic-angular';

import { todo, comment } from '../../data/todo';
import { list } from '../../data/list';
import { ListChooserPage } from '../listControls/listChooserPage';

import * as moment from 'moment';

@Component({
    selector: 'edit-task',
    templateUrl: 'editTask.html'
})
export class EditTask {
    // public lists: list[] = TestData.testLists;
    public currentList: list;
    public currentTask: todo;

    public title: string;
    public description: string;
    public comments: comment[] = [];
    public commentInput: string;

    public dueDate: string = "";

    constructor(public viewCtrl: ViewController,
        public popoverCtrl: PopoverController,
        public navParams: NavParams) {

        if (navParams.data["currentList"]) {
            this.currentList = navParams.data["currentList"];
        }

        if (navParams.data["currentTask"]) {
            this.currentTask = navParams.data["currentTask"];
            this.title = this.currentTask.header;
            this.description = this.currentTask.description;
            this.dueDate = moment(this.currentTask.due).utc().format('YYYY-MM-DDTHH:mm');
            //this.dueDate = moment.utc(this.currentTask.due).format("YYYY-MM-DD HH:mm");
            if (this.currentTask.comments) {
                this.comments = this.currentTask.comments;
            }
            //this.dueDate =  this.currentTask.due.toLocaleString("YYYY-MM-DD");
            this.currentList = this.currentTask.list;
        }
    }


    openListChooser(myEvent) {
        let popover = this.popoverCtrl.create(ListChooserPage, { showAll: false });
        popover.onDidDismiss(chosenList => {
            if (chosenList !== null) {
                this.currentList = chosenList;
            }
        });
        popover.present({
            ev: myEvent
        });
    }

    openAssigneeChooser(myEvent) {
        alert("Not implemented yet");
    }

    dismiss() {
        this.currentTask.header = this.title;
        this.currentTask.description = this.description ? this.description : null;
        if (this.dueDate && this.dueDate !== "Invalid date") {
            this.currentTask.due = moment(this.dueDate).toDate();
        }

        this.currentTask.list = this.currentList;
        this.currentTask.comments = this.comments ? this.comments : null;
        this.viewCtrl.dismiss(this.currentTask);
    }

    onNewComment() {
        if(this.commentInput && this.commentInput.length > 0) {
            var newComment: comment = { header: this.commentInput, userName: null };
            if (!this.comments) {
                this.comments = [];
            }
            this.comments.push(newComment);
            this.commentInput = "";
        }
    }
}