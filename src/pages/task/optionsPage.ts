import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';

//import { list } from '../data/list';

@Component({
  template: `
    <ion-list>
      <ion-list-header>View Options</ion-list-header>
      <ion-item>
        <ion-label>Show Completed</ion-label>  
        <ion-checkbox [(ngModel)]="showComplete" (ionChange)="close()"></ion-checkbox>
      </ion-item>
    </ion-list>

    <ion-list radio-group [(ngModel)]="sortByDate">
      <ion-list-header>
        Sort By
      </ion-list-header>
      <ion-item>
        <ion-label>Due Date</ion-label>
        <ion-radio [value]="true" [checked]="sortByDate" (ionSelect)="close()"></ion-radio>
      </ion-item>

      <ion-item>
        <ion-label>Alphabetical</ion-label>
        <ion-radio [value]="false" [checked]="!sortByDate" (ionSelect)="close()"></ion-radio>
      </ion-item>
    </ion-list>

  `
})
export class optionsPage {
  showComplete: boolean;
  sortByDate: boolean;
  

  constructor(public viewCtrl: ViewController, public navParams: NavParams, private firebaseProvider: FirebaseProvider) {
    this.showComplete = navParams.data["showComplete"];
    this.sortByDate = navParams.data["sortByDate"];
  }

  ionViewDidLoad() {
    //this.firebaseProvider.getLists();
  }


  close() {
    this.viewCtrl.dismiss({ showComplete: this.showComplete, sortByDate: this.sortByDate });
  }
}