import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { User } from "../../data/user";
import { AngularFireAuth } from 'angularfire2/auth';

import { TabsPage } from '../tabs/tabs';

import { AuthService } from '../authentication/auth.service'
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string;
  password: string;

  user: Observable<User>;

  constructor(private afAuth: AuthService,
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    public navParams: NavParams) {
    this.afAuth.user.subscribe(user => {
      if (user) {
        this.afAuth.updateUserData(user);
        this.navCtrl.setRoot(TabsPage);
      }
    });
  }


  loginGoogle() {
    this.afAuth.googleLogin();

    // try {
    //   const result = await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
    //   if (result) {
    //     this.navCtrl.setRoot(TabsPage);

    //   }
    // }
    // catch (e) {
    //   console.error(e);
    // }
  }

  loginFacebook() {
    this.showAlert();
    //this.afAuth.facebookLogin();
  }

  async loginUsernamePassword() {
    this.showAlert();
    // const result = await this.afAuth.signInEmailPassword(this.email, this.password);
    // if (result) {
    //   this.navCtrl.setRoot(TabsPage);
    // }
  }

  async register(user: User) {
    this.showAlert();
    // const result = await this.afAuth.registerUser(this.email, this.password);
    // if (result) {
    //   this.navCtrl.setRoot(TabsPage);
    // }

  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Not implemented yet...',
      subTitle: 'Only google login works at the moment',
      buttons: ['Oh well']
    });
    alert.present();
  }

  //signOut() {
  // this.afAuth.auth.signOut().then(function () {
  //   // Sign-out successful.
  // }).catch(function (error) {
  //   // An error happened.
  // });
  //}
}