import { Injectable } from '@angular/core';
//import { Router } from '@angular/core';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Platform } from 'ionic-angular';

import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { GooglePlus } from '@ionic-native/google-plus';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';

import { User } from '../../data/user';

interface credential {
    user: User,
    password: string
}

@Injectable()
export class AuthService {
    public user: Observable<User>;

    constructor(private afAuth: AngularFireAuth,
        private gplus: GooglePlus,
        private platform: Platform,
        private afs: AngularFirestore) {
        this.user = this.afAuth.authState
            .switchMap(user => {
                if (user) {
                    var retrievedUser = this.afs.doc<User>(`users/${user.uid}`).valueChanges();
                    if(retrievedUser) {
                        return retrievedUser;
                    } else {
                        this.updateUserData(user);
                    }
                    
                } else {
                    return Observable.of(null);
                }
            });

    }

    //#region Google Login

    async googleLogin() {
        if (this.platform.is('cordova')) {
          await this.nativeGoogleLogin();
        } else {
          await this.webGoogleLogin();
        }
      }

    async nativeGoogleLogin(): Promise<void> {
        try {
            const gplusUser = await this.gplus.login({
                'webClientId': '1026089166222-b47aor9eso7mq7k69g1g9p3aqnq0q0q1.apps.googleusercontent.com',
                'offline': true,
                'scopes': 'profile email'
            })

            const credential = await this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken))
            this.updateUserData(credential.user);

        } catch (err) {
            alert(err);
            console.log(err)
        }
    }

    async webGoogleLogin(): Promise<void> {
        try {
            const provider = new firebase.auth.GoogleAuthProvider();
            const credential = await this.afAuth.auth.signInWithPopup(provider);
            this.updateUserData(credential.user);

        } catch (err) {
            console.log(err)
        }

    }

    //#endregion

    async registerUser(email: string, password: string) {
        try {
            const result = await this.afAuth.auth.createUserWithEmailAndPassword(
              email,
              password,
            );
            if (result) {
                this.updateUserData(result.user);
              return true;
            }
          } catch (e) {
            console.error(e);
          }
    }

    async signInEmailPassword(email: string, password: string) {
        try {
            const result = await this.afAuth.auth.signInWithEmailAndPassword(
              email,
              password,
            );
            if (result) {
                this.updateUserData(result.user);
              return true;
            }
          } catch (e) {
            console.error(e);
          }
    }

    // googleLogin() {
    //     const provider = new firebase.auth.GoogleAuthProvider();
    //     return this.oAuthLogin(provider);
    // }

    // facebookLogin() {
    //     const provider = new firebase.auth.FacebookAuthProvider();
    //     return this.oAuthLogin(provider);
    // }

    // private oAuthLogin(provider) {
    //     return this.afAuth.auth.signInWithPopup(provider)
    //         .then((credential) => {
    //             this.updateUserData(credential.user);

    //         });
    // }

    logOut() {
        return this.afAuth.auth.signOut();
    }

    public updateUserData(user: User) {
        const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

        const data: User = {
            uid: user.uid,
            email: user.email,
            firstName: user.firstName ? user.firstName : "",
            lastName: user.lastName ? user.lastName : "",
            photoURL: user.photoURL ? user.photoURL : "",
        }

        return userRef.set(data);
    }
}