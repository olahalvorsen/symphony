import { Component } from '@angular/core';

import { TasksPage } from '../task/tasks';
import { ActivityPage } from '../activity/activity';
import { SettingsPage } from '../settings/settings';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Invitation } from '../../data/invitation';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = TasksPage;
  tab2Root = ActivityPage;
  tab3Root = SettingsPage;

  constructor(public firebaseProvider: FirebaseProvider) {
    
  }
}
