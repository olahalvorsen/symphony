import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Invitation } from '../../data/invitation';

@Component({
  selector: 'page-activity',
  templateUrl: 'activity.html'
})
export class ActivityPage {

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public fireBaseProvider: FirebaseProvider
  ) {

  }

  acceptInvite(invitation: Invitation) {

    this.fireBaseProvider.acceptInvite(invitation);
  }

}
