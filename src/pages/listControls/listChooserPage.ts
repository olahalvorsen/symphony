import { Component } from '@angular/core';
import { ViewController, ModalController, NavParams, AlertController  } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';

import { list } from '../../data/list';
import { ListSettingsPage } from './listSettingsPage';

@Component({
  templateUrl: 'listChooserPage.html',
  selector: 'list-chooser-page'
})
export class ListChooserPage {
  public showAllLists: boolean;


  constructor(public viewCtrl: ViewController,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private firebaseProvider: FirebaseProvider) {
    
  }

  addNewList() {

    let alert = this.alertCtrl.create({
      title: 'Create New List',
      inputs: [
        {
          name: 'listName',
          placeholder: 'List Name...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            this.close(null);
          }
        },
        {
          text: 'Create',
          handler: data => {
            if(data.listName && data.listName.trim().length > 0) {
              this.firebaseProvider.createList(data.listName);
              //this.close();
            }
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidLoad() {
    this.showAllLists = this.navParams.data["showAll"];
    this.firebaseProvider.getLists();
  }


  close(chosenList) {
    this.viewCtrl.dismiss(chosenList);
  }

  listSettings(currentList: list) {
    let profileModal = this.modalCtrl.create(ListSettingsPage, { currentList: currentList });
    profileModal.onDidDismiss(editedTask => {
      // if(editedTask) {
      //   this.saveChanges(editedTask);
      // }
    });

    profileModal.present();
  }
}
