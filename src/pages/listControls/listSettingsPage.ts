import { Component } from '@angular/core';
import { ViewController, NavParams, AlertController, ToastController, NavController, ModalController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';

import { list } from '../../data/list';
import { TabsPage } from '../tabs/tabs';

import { InvitePage } from '../../pages/invite/invite';

@Component({
    templateUrl: 'listSettingsPage.html',
    selector: 'list-settings-page'
})
export class ListSettingsPage {

    public currentList: list;
    public currentMembers: string[] = [];

    constructor(public navParams: NavParams,
        private navCtrl: NavController,
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        private toastCtrl: ToastController,
        private viewCtrl: ViewController,
        private firebaseProvider: FirebaseProvider) {
        if (navParams.data["currentList"]) {
            this.currentList = navParams.data["currentList"];
            this.populateMembers();
        }
    }

    populateMembers() {
        let members = Object.keys(this.currentList.members);
        for (let member of members) {
            let userEmail = this.firebaseProvider.getUserById(member).subscribe(user => {
                if(user) {
                    this.currentMembers.push(user.email);
                }
            })  
        }
    }

    presentInviteMember() {
        let profileModal = this.modalCtrl.create(InvitePage);
        profileModal.onDidDismiss(invitation => {
            if (invitation) {
                invitation.listId = this.currentList.id;
                invitation.listName = this.currentList.name;
                this.firebaseProvider.createInvite(invitation);
                let toast = this.toastCtrl.create({
                    message: 'Invitation sent to: ' + invitation.toEmail,
                    duration: 5000
                });
                toast.present();
                this.saveListSettings();
            }
        });

        profileModal.present();
    }

    cancelChanges() {
        this.viewCtrl.dismiss(this.currentList);
    }

    deleteList() {
        let alert = this.alertCtrl.create({
            title: 'Deleting list',
            message: 'Are you sure you want to delete this list?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: () => {

                    }
                },
                {
                    text: 'Yes, delete it!',
                    handler: () => {
                        this.firebaseProvider.deleteList(this.currentList);
                        this.navCtrl.setRoot(TabsPage);
                    }
                }
            ]
        });
        alert.present();


    }

    saveListSettings() {
        this.firebaseProvider.saveListChanges(this.currentList);
        this.viewCtrl.dismiss(this.currentList);
    }
}