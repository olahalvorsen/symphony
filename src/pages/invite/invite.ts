import { Component } from '@angular/core';
import { ViewController, NavParams, AlertController, NavController, ModalController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Invitation } from '../../data/invitation';

@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
})
export class InvitePage {

  invitation: Invitation;
 

  constructor(public navCtrl: NavController,
              private viewCtrl: ViewController,
              public navParams: NavParams,
              private firebaseProvider: FirebaseProvider) {
    this.invitation = { fromEmail: "",
                        toEmail: null,
                        listId: "",
                        listName: "",
                        accepted: false,
                        declined: false}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitePage');
  }

  cancelInvite() {
    this.viewCtrl.dismiss();
  }

  inviteAndClose() {
    this.viewCtrl.dismiss(this.invitation);
  }

}
