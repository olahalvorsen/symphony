import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from '../authentication/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../data/user';
import { Observable } from 'rxjs/Observable';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  public userEmail: string;
  public userImage: string;

  constructor(public navCtrl: NavController,
              private authService: AuthService,
              private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(user => {
      if(user) {
        this.userEmail = user.email;
        this.userImage = user.photoURL;
      }
    })
  }

  logOut() {
    this.authService.logOut();
    //this.navCtrl.setRoot(LoginPage);
  }
}
