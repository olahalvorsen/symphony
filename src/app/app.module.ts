import { Pro } from '@ionic/pro';
import { NgModule, ErrorHandler, Injectable, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { GooglePlus } from '@ionic-native/google-plus';

import { ActivityPage } from '../pages/activity/activity';
import { SettingsPage } from '../pages/settings/settings';
import { TasksPage } from '../pages/task/tasks';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPageModule } from '../pages/login/login.module';
import { LoginPage } from '../pages/login/login';


import { ListChooserPage } from '../pages/listControls/listChooserPage';
import { ListSettingsPage } from '../pages/listControls/listSettingsPage';

import { snoozePage } from '../controls/snooze';
import { optionsPage } from '../pages/task/optionsPage';

import { InvitePage } from '../pages/invite/invite';
import { EditTask } from '../pages/task/editTask';
import { Comments } from '../controls/comments';
import { TaskInfo } from '../controls/taskInfo';

import { AngularFireModule } from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthService } from '../pages/authentication/auth.service';

import { AutosizeDirective } from '../directives/autosize/autosize';

var firebaseConfig = {
  apiKey: "AIzaSyAiOjlhYneHiFSS_0IG2dhE4SPsKxqOk6w",
  authDomain: "symphony-e6a6a.firebaseapp.com",
  databaseURL: "https://symphony-e6a6a.firebaseio.com",
  projectId: "symphony-e6a6a",
  storageBucket: "symphony-e6a6a.appspot.com",
  messagingSenderId: "1026089166222"
};

Pro.init('2cd928c6', {
  appVersion: '0.0.1'
})

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch(e) {
      // Unable to get the IonicErrorHandler provider, ensure
      // IonicErrorHandler has been added to the providers list below
    }
  }

  handleError(err: any): void {
    Pro.monitoring.handleNewError(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
    ActivityPage,
    SettingsPage,
    TasksPage,
    TabsPage,
    ListChooserPage,
    ListSettingsPage,
    snoozePage,
    optionsPage,
    EditTask,
    InvitePage,
    Comments,
    TaskInfo,
    AutosizeDirective
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    LoginPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ActivityPage,
    SettingsPage,
    TasksPage,
    TabsPage,
    ListChooserPage,
    ListSettingsPage,
    snoozePage,
    optionsPage,
    EditTask,
    InvitePage,
    LoginPage,
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseProvider,
    AuthService,
    GooglePlus,
    IonicErrorHandler,
    [{provide: ErrorHandler, useClass: MyErrorHandler }]
  ]
})
export class AppModule {}
