import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { todo } from '../../data/todo';
import { list } from '../../data/list';
import { Invitation } from '../../data/invitation';
import { User, UserInfo } from '@firebase/auth-types';

@Injectable()
export class FirebaseProvider {
  
  //TODOS
  public todosCollection: AngularFirestoreCollection<todo>;
  public todos: Observable<todo[]>;

  //LISTS
  public listsCollection: AngularFirestoreCollection<list>;
  public lists: Observable<list[]>;

  //USER
  user: User;

  //INVITATIONS
  public invitationCollection: AngularFirestoreCollection<Invitation>;
  public invitationList: Observable<Invitation[]>;
  public invitationCounter: number = 0;

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        this.getLists();
        this.getInvitations();
      }
    })
  }



  // constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth,) {
  // }

  getTodosForList(chosenList: list, showComplete: boolean, orderByDate: boolean) {

    var orderByFilter: string = orderByDate ? "due" : "header";


    if (chosenList === null) {

      if (showComplete) {
        this.todosCollection = this.afs.collection('todos', ref => ref.where('owner', '==', this.user.uid).orderBy(orderByFilter, 'asc'));
      } else {
        this.todosCollection = this.afs.collection('todos', ref => ref.where('owner', '==', this.user.uid).where('complete', '==', false).orderBy(orderByFilter, 'asc'));
      }
    }
    else {
      if (showComplete) {
        this.todosCollection = this.afs.collection('todos', ref => ref.where('list.id', '==', chosenList.id).orderBy(orderByFilter, 'asc'))
      } else {
        this.todosCollection = this.afs.collection('todos', ref => ref.where('list.id', '==', chosenList.id).where('complete', '==', false).orderBy(orderByFilter, 'asc'))
      }
    }

    this.todos = this.todosCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as todo;
        const id = a.payload.doc.id;

        return { id, ...data };
      });
    });
  }

  getLists() {
    this.listsCollection = this.afs.collection('lists', ref => ref.where('members'.concat('.', this.user.uid), '==', true));

    this.lists = this.listsCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as list;
        const id = a.payload.doc.id;
        return { id, ...data };
      });
    });
  }

  createList(listName: string) {
    var newList: list = { name: listName, colour: 'red', logo: "", members: {} };
    this.joinList(newList);
    this.listsCollection.add(newList);
  }

  saveListChanges(changedList: list) {
    this.listsCollection.doc(changedList.id).update(changedList);
  }

  deleteList(listToDelete: list) {
    this.listsCollection.doc(listToDelete.id).delete();
  }

  createTodo(newTodo: todo) {
    this.todosCollection.add(newTodo);
  }

  saveTodoChanges(changedTodo: todo) {
    this.validateFields(changedTodo);

    this.todosCollection.doc(changedTodo.id).update(changedTodo);
  }

  deleteTodo(todoToDelete: todo) {
    this.todosCollection.doc(todoToDelete.id).delete();
  }

  validateFields(todo: todo) {
    if (todo.due) {
      todo.due = this.getDateFromString(todo.due.toString());
    }

  }

  getDateFromString(datetoSaved: string): Date {
    let date: Date = new Date(datetoSaved);
    return date;
  }

  getUserById(id: string) : Observable<UserInfo> {
    let theUser = this.afs.doc<UserInfo>(`/users/${id}`).snapshotChanges().map(a => {
      const data = a.payload.data() as UserInfo;
            const id = a.payload.id;
            return { id, ...data };
    });

    return theUser;
  }

  // getInvitationsByListId(listId: string): Observable<Invitation> {
  //   let theInvitation = this.afs.doc<Invitation>(`/invitations/${id}`).snapshotChanges().map(a => {
  //     const data = a.payload.data() as UserInfo;
  //           const id = a.payload.id;
  //           return { id, ...data };
  //   });

  //   return theUser;

  // }

  acceptInvite(invitation: Invitation) {
    let theList = this.afs.doc<list>(`/lists/${invitation.listId}`).snapshotChanges().map(a => {
      const data = a.payload.data() as list;
            const id = a.payload.id;
            return { id, ...data };
    });
      
    theList.subscribe(list => {
      this.joinList(list);
      this.saveListChanges(list);
      invitation.accepted = true;
      this.invitationCollection.doc(invitation.id).update(invitation);
    })

  }

  joinList(listToJoin: list) {
    if (!listToJoin.members[this.user.uid]) {
      listToJoin.members[this.user.uid] = true;
    }
  }

  leaveList(itemKey) {
    // const member = this.db.object(`items/${itemKey}/members/${this.userId}`)
    // member.remove()
  }

  getInvitations() {
    //this.invitationCounter = 0;
    this.invitationCollection = this.afs.collection('invitations', ref => ref.where('toEmail', '==', this.user.email));

    this.invitationList = this.invitationCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Invitation;
        const id = a.payload.doc.id;
        this.invitationCounter = this.invitationCounter + 1;
        console.log(this.invitationCounter);
        return { id, ...data };
      });
    });

    // this.invitationList.subscribe(keys => {
    //   keys.forEach(key => this.invitationCounter = this.invitationCounter + 1);
    // });

  }

  // getInvitationCount() {
  //   this.afs.collection("cities").get.then(function(querySnapshot) {      
  //     console.log(querySnapshot.size); 
  //   });
  // }

  createInvite(invitation: Invitation) {
    invitation.fromEmail = this.user.email;
    this.invitationCollection.add(invitation);
  }
}